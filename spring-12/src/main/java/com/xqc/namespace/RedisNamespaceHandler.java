package com.xqc.namespace;

import com.xqc.parse.RedisBeanDefinitionParser;
import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class RedisNamespaceHandler extends NamespaceHandlerSupport {

    public void init() {
        this.registerBeanDefinitionParser("redis",new RedisBeanDefinitionParser());
    }
}
