package com.xqc;

import com.xqc.redis.RedisTag;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Spring12 {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        RedisTag redis = applicationContext.getBean(RedisTag.class);
        System.out.println(redis.getIp()+"\t"+redis.getPort()+"\t"+redis.getDesc());
    }
}
