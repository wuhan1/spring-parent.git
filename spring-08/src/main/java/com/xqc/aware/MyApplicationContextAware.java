package com.xqc.aware;

import com.xqc.bean.Stu;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class MyApplicationContextAware implements ApplicationContextAware, BeanNameAware {

    //利用ApplicationContextAware可以获取到IOC容器
    ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("============"+applicationContext);
        Stu stu = (Stu) applicationContext.getBean("stu");
        System.out.println(stu);
        this.applicationContext = applicationContext;
    }

    @Override
    public void setBeanName(String name) {
        //获取当前bean的名字
        System.out.println("===================="+name);
        //====================myApplicationContextAware
    }
}
