package com.xqc.config;

import com.xqc.bean.Dev;
import com.xqc.bean.Pro;
import com.xqc.bean.Stu;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.xqc")//扫描的包
public class SpringConfig {

    @Bean
    public Stu stu(){
        return new Stu();
    }

    @Profile("pro")
    @Bean
    public Pro pro(){
        return new Pro();
    }

    @Bean
    @Profile("dev")
    public Dev dev(){
        return new Dev();
    }
}
