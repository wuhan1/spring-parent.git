package com.xqc;

import com.xqc.bean.Dev;
import com.xqc.bean.Pro;
import com.xqc.config.SpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringTest08 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.getEnvironment().setActiveProfiles("pro");
        context.register(SpringConfig.class);
        context.refresh();

        System.out.println(context);
//        Dev dev = (Dev) context.getBean("dev");
//        System.out.println(dev);

        Pro pro = (Pro) context.getBean("pro");
        System.out.println(pro);
        context.close();

    }
}
