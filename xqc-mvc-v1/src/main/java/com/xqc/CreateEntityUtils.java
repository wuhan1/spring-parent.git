package com.xqc;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.SimpleFormatter;

public class CreateEntityUtils {

    private CreateEntityUtils(){}

    public static void CreateEntity(Map<String, String> columnFieldType,String tableName,String author){
        String replaceClassFirstWord = replaceClassFirstWord(tableName);

        StringBuilder builder = new StringBuilder("");
        builder.append("")
        .append("import com.baomidou.mybatisplus.annotations.TableId;\n")
        .append("import com.baomidou.mybatisplus.annotations.TableName;\n")
        .append("import com.baomidou.mybatisplus.enums.IdType;\n")
        .append("import com.xsyx.servicestation.jdbc.persistent.AbstractSuperEntity;\n")
        .append("import lombok.Data;\n")
        .append("import java.util.Date;\n\n")
        .append("/**\n*\n")
        .append("* @author ")
        .append(author)
        .append("\n")
        .append("* @version $Id: ").append(replaceClassFirstWord)
        .append(".java,v 0.1 ")
        .append(new SimpleDateFormat("yyyy年MM月dd日 HH:mm").format(new Date()))
        .append("   $Exp\n")
        .append("*/\n")
        .append("@Data\n@TableName(\"")
        .append(tableName)
        .append("\")\n")
        .append("public class ")
        .append(replaceClassFirstWord)
        .append("Entity ")
        .append(" extends AbstractSuperEntity<")
        .append(replaceClassFirstWord)
        .append("Entity> {\n");
        for (Map.Entry<String, String> entry : columnFieldType.entrySet()) {
            String fieldType = entry.getValue();
            System.out.println(entry.getKey()+"================"+fieldType);
            if(Timestamp.class.getName().equals(fieldType)){
                builder.append("private Date ").append(entry.getKey()).append(";\n");
            }else if(String.class.getName().equals(fieldType)){
                builder.append("private String ").append(entry.getKey()).append(";\n");
            }else if(BigDecimal.class.getName().equals(fieldType)){
                builder.append("private Double ").append(entry.getKey()).append(";\n");
            }else if(Long.class.getName().equals(fieldType)){
                builder.append("private Long ").append(entry.getKey()).append(";\n");
            }else if(Integer.class.getName().equals(fieldType)){
                builder.append("private Integer ").append(entry.getKey()).append(";\n");
            }
        }

        builder.append("}");
        System.out.println(builder.toString());
    }

    /**
     * 根据表名称生成实体类
     * @param tableName
     * @return
     */
    private static String replaceClassFirstWord(String tableName){
        String subTableName = tableName.replace("t_", "");
        String[] splitStrs = subTableName.split("_");
        StringBuilder builder = new StringBuilder();
        for (String splitStr : splitStrs) {
            char[] charArray = splitStr.toCharArray();
            charArray[0] = Character.toUpperCase(charArray[0]);
            builder.append(new String(charArray));
        }
        return builder.toString();
    }
}
