package com.xqc;

public class EmptyUtils {

	private EmptyUtils(){}
	
	public static boolean isEmpty(String str) {
		return str == null || str.length() < 1;
	}
	
}
