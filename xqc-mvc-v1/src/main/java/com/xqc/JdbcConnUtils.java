package com.xqc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Gavin.Dai
 * @author SNSSZ00036
 */
public class JdbcConnUtils {

	private static final Logger logger = LoggerFactory.getLogger(JdbcConnUtils.class);
	protected static final Properties  configProperties = XqcReadFileUtils.configProperties;
	private static String url = null;
	private static String userName = null ;
	private static String passWord = null ;
	private JdbcConnUtils(){}
	
	/**
	 * 初始化连接数大小
	 */
	static{
		  try {
			url = configProperties.getProperty("jdbc.url").trim();
			userName = configProperties.getProperty("jdbc.username").trim();
			passWord = configProperties.getProperty("jdbc.password").trim();
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			logger.error("初始化数据库连接失败",e);
		}
	}
	
	
	/**
	 * 新建一个连接
	 * @return
	 * @throws SQLException 
	 */
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url,userName ,passWord);
	}
	
}
