package com.xqc;

import lombok.Data;

import java.io.Serializable;

@Data
public class TableDefinition implements Serializable {
    /**
     * 字段名称
     */
    private String fieldName;
    /**
     * 字段类型
     */
    private String fieldType;
    /**
     * 是否主键
     */
    private String key;
    /**
     * 字段备注
     */
    private String comment;
}
