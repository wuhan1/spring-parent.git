package com.xqc;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {

	private CommonUtils(){}
	
	/**
	 * Object转字符串
	 */
	public static String objectToString(Object value){
		return value == null?null:String.valueOf(value);
	}
	
	/**
	 * 字符串转Double
	 */
	public static Double stringToDouble(String value){
		return EmptyUtils.isEmpty(value)?null:Double.valueOf(value);
	}
	
	/**
	 * 时间识别
	 */
	public static Timestamp toTimestamp(Object value){
		if(value == null){
			return null;
		}
		if(value instanceof Date){
			return dateToTimestamp(value);
		}
		return objectToTimestamp(value);
	}
	
	/**
	 * 字符串格式的日期
	 */
	public static Timestamp objectToTimestamp(Object value){
		if(value == null){
			return null;
		}
		Date date = formatToDate(value.toString());
		if(date == null){
			return null;
		}
		return new Timestamp(date.getTime());
	}
	
	/**
	 * 日期格式
	 */
	public static Timestamp dateToTimestamp(Object value){
		if(value == null){
			return null;
		}
		if(value instanceof Date){
			return new Timestamp(((Date)value).getTime());
		}
		return null;
	}
	
	/**
	 * object转BigDecimal
	 */
	public static BigDecimal objectToBigDecimal(Object value){
		return value == null?null:new BigDecimal(value.toString());
	}
	
    public static final String FORMAT_DATE_YMDHMS = "yyyy-MM-dd HH:mm:ss";
   	/**
     *年月日时分时间格式
     * */
    public static final String FORMAT_DATE_YMDHM = "yyyy-MM-dd HH:mm";
    /**
     *  年月日时间格式
     */
    public static final String FORMAT_DATE_YMD= "yyyy-MM-dd";
    /**
     * 年月日时分秒时间格式正则匹配
     */
	public static final String DATE_REGEX_1 = "[1-9]\\d{3}-((0[1-9])|(1[012]))-((0[1-9])|([12]\\d)|(3[01])) (((0|1)\\d)|(2[0-3])):([0-5]\\d):([0-5]\\d)";
	/**
	 * 年月日时分时间格式正则匹配
	 */
	public static final String DATE_REGEX_2 = "[1-9]\\d{3}-((0[1-9])|(1[012]))-((0[1-9])|([12]\\d)|(3[01])) (((0|1)\\d)|(2[0-3])):([0-5]\\d)";
	/**
	 * 年月日时间格式正则匹配
	 */
	public static final String DATE_REGEX_3 = "[1-9]\\d{3}-((0[1-9])|(1[012]))-((0[1-9])|([12]\\d)|(3[01]))";
	
	public static Date formatToDate(String strDate){
		if(EmptyUtils.isEmpty(strDate)){
			return null;
		}
		try {
			if(strDate.matches(DATE_REGEX_1)){
				return  new SimpleDateFormat(FORMAT_DATE_YMDHMS).parse(strDate);
			}else if(strDate.matches(DATE_REGEX_2)){
				return  new SimpleDateFormat(FORMAT_DATE_YMDHM).parse(strDate);
			}else if(strDate.matches(DATE_REGEX_3)){
				return  new SimpleDateFormat(FORMAT_DATE_YMD).parse(strDate);
			}
			return null;
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static String formatDateYmdhms(Date date){
		if(date == null){
			return "";
		}
		return  new SimpleDateFormat(FORMAT_DATE_YMDHMS).format(date);
	}
	
}
