package com.xqc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XqcReadFileUtils {
	
	private XqcReadFileUtils(){}

	protected static final Properties  configProperties = new Properties();
	
	private static  final Logger logger = LoggerFactory.getLogger(XqcReadFileUtils.class);
	
	public static void readFile(){
		InputStream inputStream = null;
		try {
			String rootPath = System.getProperty("user.dir");
			 inputStream = new FileInputStream(new File(rootPath+"/xqc-mvc-v1/jdbc.properties"));
			configProperties.load(inputStream);
			logger.info("======数据库配置文件根路径========{}",rootPath);
		} catch (IOException e) {
			IOUtils.closeQuietly(inputStream);
			logger.error("读取数据库配置异常",e);
		}
	}
}
