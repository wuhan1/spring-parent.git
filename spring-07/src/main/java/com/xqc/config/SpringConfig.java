package com.xqc.config;

import com.xqc.service.Addr;
import com.xqc.service.Stu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.xqc")
public class SpringConfig {

    @Bean
    public Stu stu(@Autowired Addr addr){
        System.out.println(addr);
        return new Stu();
    }

    @Bean
    public Addr addr(){
        return new Addr();
    }
}
