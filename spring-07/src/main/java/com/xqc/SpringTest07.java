package com.xqc;

import com.xqc.config.SpringConfig;
import com.xqc.service.StuServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class SpringTest07 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        System.out.println(Arrays.toString(context.getBeanDefinitionNames()));
        StuServiceImpl stuService = (StuServiceImpl) context.getBean("stuService");
        stuService.add();
    }
}
