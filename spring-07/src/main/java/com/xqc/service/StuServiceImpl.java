package com.xqc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("stuService")
public class StuServiceImpl {

//    @Resource(type =StuDaoImpl2.class )
    @Autowired
    @Qualifier("stuDaoImpl")
    private StuDao stuDao;

//    @Autowired
//    private  StuServiceImpl(StuDao stuDao){
//        this.stuDao = stuDao;
//    }
//    public void setStuDao( StuDao stuDao) {
//        System.out.println("set方法");
//        this.stuDao = stuDao;
//    }

    public void add(){
        System.out.println("==="+stuDao);
        this.stuDao.add();
    }
}
