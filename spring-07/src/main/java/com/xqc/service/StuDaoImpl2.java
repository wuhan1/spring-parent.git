package com.xqc.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Primary
@Repository("stuDaoImpl2")
public class StuDaoImpl2 implements StuDao{

    @Override
    public void add(){
        System.out.println("增加学生。。。");
    }
}
