package com.xqc.anno;

import com.xqc.fac.AppleFru;
import com.xqc.fac.MyFactoryBean;
import com.xqc.service.PcPrintServiceImpl;
import com.xqc.service.PrintService;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AnnConfig {

//    @Bean("pcPrintServiceImpl")
    @Bean
    public PrintService pcPrintServiceImpl(){
        return new PcPrintServiceImpl();
    }

    @Bean
    public FactoryBean<AppleFru> myFactoryBean(){
        return new MyFactoryBean();
    }
}
