package com.xqc;

import com.xqc.anno.AnnConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringTest {
    public static void main(String[] args) {
//        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//        System.out.println(context.getBean("stuImpl"));
//        System.out.println(context.getBean("stuImpl"));

        //注解方式
        ApplicationContext context = new AnnotationConfigApplicationContext(AnnConfig.class);
        Object myFactoryBean = context.getBean("myFactoryBean");
        System.out.println(myFactoryBean);
        Object myFactoryBean2 = context.getBean("&myFactoryBean");
        System.out.println(myFactoryBean2);
    }
}
