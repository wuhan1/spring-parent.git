package com.xqc.ann;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AnnAspectj {

    /**
     * 前置通知
     */
    @Before("execution(* com.xqc.com.StudentServiceImpl.*(..))")
    public void before(JoinPoint joinPoint){
        System.out.println("注解通知，before。。。。");
    }

    /**
     * 最终通知
     */
    @AfterReturning("execution(* com.xqc.com.StudentServiceImpl.*(..))")
    public void After(JoinPoint joinPoint){
        System.out.println("注解通知，AfterReturning。。。。");
    }

    /**
     * 最终通知
     */
    @Around("execution(* com.xqc.com.StudentServiceImpl.*(..))")
    public Object Around(ProceedingJoinPoint joinPoint){
        System.out.println("注解通知，Around之前。。。。");
        Object proceed = null;
        try {
            proceed = joinPoint.proceed();
            System.out.println("注解通知，Around之后。。。。");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return proceed;
    }

}
