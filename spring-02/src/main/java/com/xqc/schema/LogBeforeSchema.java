package com.xqc.schema;

import org.aspectj.lang.JoinPoint;

/**
 * schema方式
 */
public class LogBeforeSchema {

    public void before(JoinPoint joinPoint){
        System.out.println("schema前置通知");
    }

    public void afterReturning(JoinPoint joinPoint){
        System.out.println("schema后置通知");
    }
}
