package com.xqc.xml;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.ThrowsAdvice;

import java.lang.reflect.Method;

/**
 * 异常通知
 * aop的一个切面接口是 ThrowsAdvice，这是个标记接口，里面没有定义任何方法。书上说，根据spring文档，必须定义一个 afterThrowing([Method, args, target], subclassOfThrowable) 形式的方法，前面三个参数可选，也就是你可以写成 afterThrowing( args, target, subclassOfThrowable) ，也可以写成 afterThrowing( target, subclassOfThrowable)
 *
 * 事实上如果真的这么做，运行时会抛出 At least one handler method must be found in class 形式的异常。在确认自己没有打错字之后，只好去查spring2.0的手册，才发现上面是这么说的：方法可以有一个或四个参数。 也就是说，不能有两个、三个参数，方法的形式只能有两种： afterThrowing([Method, args, target], subclassOfThrowable)  或者 afterThrowing( subclassOfThrowable)
 */
public class LogThrow implements ThrowsAdvice {

        public void afterThrowing(Method method, Object args, Object target, Throwable throwable){
            System.out.println("异常通知：吃饭中途肚子不舒服....."+method.getName());
        }
}
