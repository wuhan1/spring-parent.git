package com.xqc.xml;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * 环绕通知
 */
public class LogAround implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        Object proceed = null;
        System.out.println("环绕通知：吃饭吃饭前后我都可以记录....."+methodInvocation.getMethod().getName());
        System.out.println("环绕通知：饭前");
        try{
             proceed = methodInvocation.proceed();
            System.out.println("环绕通知：饭后");
        }catch (Exception e){
            System.out.println("环绕通知：中途不舒服"+e.getMessage());
        }
        return proceed;
    }
}
