package com.xqc;

import com.xqc.com.StudentServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {

    public static void main(String[] args) {
        //xml的方式配置aop通知
        String resourceName = "applicationContext.xml";
        //注解方式
         resourceName = "annotation-applicationContext.xml";
         //schema方式
         resourceName = "schema-applicationContext.xml";
        ApplicationContext context = new ClassPathXmlApplicationContext(resourceName);
        StudentServiceImpl studentServiceImpl = (StudentServiceImpl) context.getBean("studentServiceImpl");
        studentServiceImpl.eat();
        System.out.println("============================================================");
//        studentServiceImpl.unComfortable();
    }

}
