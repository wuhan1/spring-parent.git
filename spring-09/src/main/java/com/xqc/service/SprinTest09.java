package com.xqc.service;

import javafx.application.Application;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

public class SprinTest09 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Object stuService = context.getBean("stuService");
        ((ClassPathXmlApplicationContext) context).close();

//        BeanFactory context = ne XmlBeanFactory(new ClassPathwResource("applicationContext.xml"));
//        Object stuService = context.getBean("stuService");
    }

}
