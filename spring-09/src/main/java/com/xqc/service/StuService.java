package com.xqc.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

public class StuService implements BeanNameAware, BeanFactoryAware, ApplicationContextAware, InitializingBean, DisposableBean {

    private String desc;

    public void myInit(){
        System.out.println("自定义init-method的bean初始化");
    }

    public void myDestroy(){
        System.out.println("自定义destroy-method的bean销毁");
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        System.out.println("setDesc方法。。。");
        this.desc = desc;
    }

    public StuService(){
        System.out.println("无参构造器...");
    }

    @Override
    public void setBeanName(String beanName) {
        System.out.println("BeanNameAware接口setBeanName方法 获取正在实例化的bean名称"+beanName);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("BeanFactoryAware接口setBeanFactory方法 获取bean工厂"+beanFactory);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("ApplicationContextAware接口setApplicationContext方法获取上下文"+applicationContext);
    }

    @Override
    public String toString() {
        return "StuService{" +
                "desc='" + desc + '\'' +
                '}';
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("实现InitializingBean接口的afterPropertiesSet，初始化bean");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("实现DisposableBean接口的destroy，销毁bean");
    }
}
