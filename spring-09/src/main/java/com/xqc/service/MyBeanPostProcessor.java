package com.xqc.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * bean后置处理器，可以用来实现下面的功能
 * 1.记录每个对象实例化的时间
 * 2.过滤每个调用对象IP
 * 3.给对象加属性
 */
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessBeforeInitialization bean名称——》"+beanName+"\tbean对象——》"+bean);
        if(bean instanceof  StuService){
            StuService stuService = (StuService) bean;
            stuService.setDesc("哈哈哈哈哈哈，修改了属性的值");
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization bean名称——》"+beanName+"\tbean对象——》"+bean);
        return bean;
    }
}
