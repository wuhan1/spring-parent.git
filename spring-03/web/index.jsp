<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<title>IT园_编程爱好者学习交流的平台</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta name="Keywords"
	content="Java、C、.NET、PHP、C#、JACASCRIPT、JQUERY、大数据、HTML、HTML5、CSS、PYTHON、编程、软件开发">
	<meta name="description"
	content="为广大爱好者提供java、C、.NET、PHP、C#、JACASCRIPT、JQUERY、大数据、HTML、HTML5、CSS、PYTHON等语言的交流论坛和各种资源下载">
	<meta http-equiv="refresh" content="0; url=index">
  </head>
</html>
