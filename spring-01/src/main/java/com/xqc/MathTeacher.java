package com.xqc;
//数学老师
public class MathTeacher {
    //姓名
    private String tccName;
    //年龄
    private Integer age;

    public String getTccName() {
        return tccName;
    }

    public void setTccName(String tccName) {
        this.tccName = tccName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public MathTeacher(String tccName, Integer age) {
        this.tccName = tccName;
        this.age = age;
    }

    public MathTeacher() {
    }

    @Override
    public String toString() {
        return "MathTeacher{" +
                "tccName='" + tccName + '\'' +
                ", age=" + age +
                '}';
    }
}
