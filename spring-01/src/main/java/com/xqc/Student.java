package com.xqc;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Student {
    //学号
    private Integer stuNo;
    //姓名
    private String stuName;
    //爱好
    private List<String> hobbies;
    //性别
    private String[] sexs[];
    //map
    private Map<String,String> mapElement;
    //set
    private Set<String> stEment;
    //数学老师
    private MathTeacher mathTeacher;
    //赋null值
    private String stNull;
    //赋值""
    private String str;

    public Integer getStuNo() {
        return stuNo;
    }

    public void setStuNo(Integer stuNo) {
        this.stuNo = stuNo;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public void setSexs(String[][] sexs) {
        this.sexs = sexs;
    }

    public void setMapElement(Map<String, String> mapElement) {
        this.mapElement = mapElement;
    }

    public MathTeacher getMathTeacher() {
        return mathTeacher;
    }

    public void setMathTeacher(MathTeacher mathTeacher) {
        this.mathTeacher = mathTeacher;
    }

    public Set<String> getStEment() {
        return stEment;
    }

    public void setStEment(Set<String> stEment) {
        this.stEment = stEment;
    }

    public Student(Integer stuNo, String stuName) {
        this.stuNo = stuNo;
        this.stuName = stuName;
    }

    public Student() {
    }

    public String getStNull() {
        return stNull;
    }

    public void setStNull(String stNull) {
        this.stNull = stNull;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stuNo=" + stuNo +
                ", stuName='" + stuName + '\'' +
                ", hobbies=" + hobbies +
                ", sexs=" + Arrays.toString(sexs) +
                "\n , mapElement=" + mapElement +
                "\n , stEment=" + stEment +
                "\n , mathTeacher=" + mathTeacher +
                "\n , stNull=" + stNull +
                ", str='" + str + '\'' +
                '}';
    }
}
