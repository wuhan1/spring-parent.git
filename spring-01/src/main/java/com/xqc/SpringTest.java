package com.xqc;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

/**
 * 读取spring配置文件的几种方式
 */
public class SpringTest {
    public static void main(String[] args) {
//        readConfigXml();

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Student student = (Student) context.getBean("student4");
        System.out.println(student);
    }

    private static void readConfigXml() {
        //方法1
//        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//        Student student = (Student) context.getBean("student");
//        System.out.println(student);

        //方法2
//        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
//        Student student = (Student) beanFactory.getBean("student");
//        System.out.println(student);

        //方法3
//        FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("/spring-01/src/main/resources/applicationContext.xml");
//        Student student = (Student) context.getBean("student");
//        System.out.println(student);


        //方法4
//        String rootPath = System.getProperty("user.dir");
//        BeanFactory context = new XmlBeanFactory(new FileSystemResource(rootPath+"/spring-01/src/main/resources/applicationContext.xml"));
//        Student student = (Student) context.getBean("student");
//        System.out.println(student);
    }
}
