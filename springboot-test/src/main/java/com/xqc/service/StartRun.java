package com.xqc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StartRun implements CommandLineRunner {

    @Autowired
    private TeacherService teacherService;

    @Override
    public void run(String... args) throws Exception {
        teacherService.run();
    }
}
