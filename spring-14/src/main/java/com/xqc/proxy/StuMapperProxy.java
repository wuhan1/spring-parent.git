package com.xqc.proxy;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class StuMapperProxy implements InvocationHandler {

    private Object object;
    public StuMapperProxy(Object object){
        this.object  = object;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        return method.invoke(object,objects);
    }
}
