package com.xqc.proxy;

import java.lang.reflect.Proxy;

public class StuMapperTest {
    public static void main(String[] args) {
        StuMapper stuMapper = new StuMapperImpl();
        StuMapperProxy stuMapperProxy = new StuMapperProxy(stuMapper);

        StuMapper proxy = (StuMapper) Proxy.newProxyInstance(StuMapperTest.class.getClassLoader(), new Class[]{StuMapper.class}, stuMapperProxy);

        proxy.say();
    }
}
