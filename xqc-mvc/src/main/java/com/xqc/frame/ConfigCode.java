package com.xqc.frame;

import com.xqc.utils.*;
import com.xqc.config.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

@Component
public class ConfigCode {
    private final static Logger logger = Logger.getLogger("ConfigDB");

    @Autowired
    private ConfigPrint configPrint;
    @Autowired
    PropertiesUtil propertiesUtil;

    private JCheckBox entityCheckBox;
    private JCheckBox requestCheckBox;
    private JCheckBox reponseCheckBox;
    private JCheckBox mapperCheckBox;
    private JCheckBox xmlCheckBox;
    private JCheckBox serviceCheckBox;
    private JCheckBox facadeCheckBox;
    private JCheckBox clientCheckBox;
    private JCheckBox controllerCheckBox;
    private JComboBox tablelistBox;
    private JTextField passwordTextField;

    public ConfigFrame cf = new ConfigFrame();

    public JPanel buildJpanel(JPanel panel) {
        int y0 = 0;
        panel.add(cf.buildJBorder("代码生成配置", 5, y0 + 10, 490, 180));

        panel.add(ConfigFrame.buildJLabel("选择表：", 15, y0 + 35, 80, 25));
        tablelistBox = cf.buildJComboBox(TableUtils.getTables(), 100, y0 + 30, 350, 25);
        panel.add(tablelistBox);

        // entity
        panel.add(ConfigFrame.buildJLabel("entity:", 15, y0 + 70, 75, 35));
        entityCheckBox = cf.buildJCheckBox(entityCheckBox, null, "entity", 20, 55, y0 + 62, 40, 40);
        panel.add(entityCheckBox);
        // request
        panel.add(ConfigFrame.buildJLabel("request:", 100, y0 + 70, 75, 35));
        requestCheckBox = cf.buildJCheckBox(requestCheckBox, null, "request", 20, 155, y0 + 62, 40, 40);
        panel.add(requestCheckBox);
        // reponse
        panel.add(ConfigFrame.buildJLabel("reponse:", 200, y0 + 70, 75, 35));
        reponseCheckBox = cf.buildJCheckBox(reponseCheckBox, null, "reponse", 20, 255, y0 + 62, 40, 40);
        panel.add(reponseCheckBox);
        // mapper
        panel.add(ConfigFrame.buildJLabel("mapper:", 300, y0 + 70, 75, 35));
        mapperCheckBox = cf.buildJCheckBox(mapperCheckBox, null, "mapper", 20, 355, y0 + 62, 40, 40);
        panel.add(mapperCheckBox);
        // xml
        panel.add(ConfigFrame.buildJLabel("xml:", 400, y0 + 70, 75, 35));
        xmlCheckBox = cf.buildJCheckBox(xmlCheckBox, null, "xml", 20, 435, y0 + 62, 40, 40);
        panel.add(xmlCheckBox);

        /************************第二排*************************/
        // service
        panel.add(ConfigFrame.buildJLabel("service:", 15, y0 + 110, 75, 35));
        serviceCheckBox = cf.buildJCheckBox(serviceCheckBox, null, "service", 20, 55, y0 + 102, 40, 40);
        panel.add(serviceCheckBox);
        // facade
        panel.add(ConfigFrame.buildJLabel("facade:", 100, y0 + 110, 75, 35));
        facadeCheckBox = cf.buildJCheckBox(facadeCheckBox, null, "facade", 20, 155, y0 + 102, 40, 40);
        panel.add(facadeCheckBox);
        // client
        panel.add(ConfigFrame.buildJLabel("client:", 200, y0 + 110, 75, 35));
        clientCheckBox = cf.buildJCheckBox(clientCheckBox, null, "client", 20, 255, y0 + 102, 40, 40);
        panel.add(clientCheckBox);
        // controller
        panel.add(ConfigFrame.buildJLabel("controller:", 300, y0 + 110, 75, 35));
        controllerCheckBox = cf.buildJCheckBox(controllerCheckBox, null, "controller", 20, 355, y0 + 102, 40, 40);
        panel.add(controllerCheckBox);

        // 添加按钮，绑定事件监听
        JButton saveButton = ConfigFrame.buildJButton("确定", 100, y0 + 150, 80, 25);
        addActionListener(saveButton);
        panel.add(saveButton);
        return panel;
    }

    // 为按钮绑定监听
    private void addActionListener(JButton clickButton) {
        clickButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if("确定".equals(clickButton.getText())){
                            saveActiveEvent();
                        }
                    }
                });
    }

    // save event
    private void saveActiveEvent() {
        String author = "";
        String tableName = tablelistBox.getSelectedItem().toString();
        //entity
        boolean entitySelected = entityCheckBox.isSelected();
        if(entitySelected){
            EntityUtils.CreateEntity(TableUtils.tableDefinitionBeans(tableName),tableName);
        }

        //facade
        boolean facadeSelected = facadeCheckBox.isSelected();
        if(facadeSelected){
            //接口
            FacadeUtils.createFacadeInf(tableName);
            //实现类
            FacadeUtils.createFacadeInfImpl(tableName);
        }

        //service
        boolean serviceSelected = serviceCheckBox.isSelected();
        if(serviceSelected){
            //实现类
            ServiceUtils.createService(tableName);
        }

        //mapper
        boolean mapperSelected = mapperCheckBox.isSelected();
        if(mapperSelected){
            MapperUtils.createMapper(tableName);
        }
        //mapperxml
        boolean xmlSelected = xmlCheckBox.isSelected();
        if(xmlSelected){
            MapperXmlUtils.createMapperXml(tableName);
        }


    }
}
