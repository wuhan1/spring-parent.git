package com.xqc.frame;

import com.xqc.config.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class ConfigFilePath {
    private final static Logger logger = LoggerFactory.getLogger(ConfigFilePath.class);
    private JTextField writeFileField;

    public JPanel buildJpanel(JPanel panel) {
        ConfigFrame cf = new ConfigFrame();
        String filePath =  PropertiesUtil.getWriteFilePath();
        int y0 = 200;
        panel.add(cf.buildJBorder("文件输出路径配置", 5, y0, 490,70));
        //添加服务器地址标签
        panel.add(ConfigFrame.buildJLabel("选择路径：", 15, y0 + 30, 80, 25));
        writeFileField = cf.buildJTextField(writeFileField, filePath, "writeFilePath", 20, 100, y0 + 30, 265, 25);
        writeFileField.setEnabled(false);
        panel.add(writeFileField);

        //添加上传按钮
        JButton linkOrBreak = ConfigFrame.buildJButton("选择", 370, y0 + 30, 80, 25);
        addActionListener(linkOrBreak);
        panel.add(linkOrBreak);

        return panel;
    }

    // 为按钮绑定监听
    private void addActionListener(JButton saveButton) {
        saveButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        if(saveButton.getText()=="选择")
                        {
                            String filePath = new UpLoadWindow().UpLoadFile(new JButton());
                            if(StringUtils.isEmpty(filePath)){
                                return;
                            }
                            writeFileField.setText(filePath);
                        }
                    }
                });
    }

}
