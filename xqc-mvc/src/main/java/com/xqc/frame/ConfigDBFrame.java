package com.xqc.frame;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

@Component
public class ConfigDBFrame {
    private final static Logger logger = Logger.getLogger("ConfigFrame");

    @Autowired
    private  ConfigDB configDB;
    @Autowired
    private  ConfigFilePath configFilePath;

    JFrame frame = new JFrame("基本信息配置");

    public  void show() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel = configDB.buildJpanel(panel,frame);
        panel = configFilePath.buildJpanel(panel);
        buildFrame(panel);
    }

    private  void buildFrame(JComponent component) {
        component.setBounds(0, 0, 660, 420);
        frame.add(component);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().setLayout(null);
        frame.getContentPane().add(BorderLayout.CENTER, component);
        // 设置窗口最小尺寸
        frame.setMinimumSize(new Dimension(660, 420));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(true);
    }

}
