package com.xqc.frame;

import com.xqc.config.PropertiesUtil;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.HashSet;

public class UpLoadWindow {
    public String UpLoadFile(JButton developer) {
//        eventOnImport(new JButton());
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(true);
        //选择目录
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle("请选择目录");
        int returnVal = chooser.showOpenDialog(developer);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            PropertiesUtil.setWriteFilePath(chooser.getSelectedFile().getAbsolutePath());
            return chooser.getSelectedFile().getAbsolutePath();
        }
        return null;
    }

    /**
     * 文件上传功能
     *
     * @param developer
     *            按钮控件名称
     */
    public static String eventOnImport(JButton developer) {
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(true);
        //选择目录
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle("请选择目录");
        int returnVal = chooser.showOpenDialog(developer);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            PropertiesUtil.setWriteFilePath(chooser.getSelectedFile().getAbsolutePath());
            return chooser.getSelectedFile().getAbsolutePath();
        }
        return null;
    }
}
