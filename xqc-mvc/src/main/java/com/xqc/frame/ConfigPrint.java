package com.xqc.frame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Writer;
import java.time.LocalDateTime;

@Component
public class ConfigPrint {
    JTextArea logTextArea;

    public ConfigFrame cf = new ConfigFrame();

    public JPanel buildJpanel(JPanel panel) {
        int y0 = 0;
        panel.add(cf.buildJBorder("日志", 500, y0 + 10, 630, 720));

        logTextArea = new JTextArea();
        panel.add(logTextArea);

        JScrollPane logScrollPane = new JScrollPane();
        logScrollPane.setBounds(510, y0 + 40, 610, 680);
        logScrollPane.getViewport().add(logTextArea);
        panel.add(logScrollPane);
//        Logger root = Logger.getRootLogger();
        Logger root = LoggerFactory.getLogger("");
        try {
//            Appender appender = root.getAppender("WriterAppender");
            PipedReader reader = new PipedReader();
            Writer writer = new PipedWriter(reader);
//            ((WriterAppender) appender).setWriter(writer);
            Thread t = new Appendered(reader, logTextArea, logScrollPane);
            t.start();
        } catch (Exception e) {
        }
        return panel;
    }

    public void printLog(String textLog){
        logTextArea.setText(LocalDateTime.now().toString()+":"+textLog+"\n"+logTextArea.getText());
    }
}
