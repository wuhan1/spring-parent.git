package com.xqc.frame;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.logging.Logger;

@Component
public class ConfigFrame {
    private final static Logger logger = Logger.getLogger("ConfigFrame");

    @Autowired
    private  ConfigDB configDB;
    @Autowired
    private  ConfigSocket configSocket;
    @Autowired
    private  ConfigEvent configEvent;
    @Autowired
    private  ConfigTask configTask;
    @Autowired
    private  ConfigPrint configPrint;
    @Autowired
    private  ConfigFilePath configFilePath;
    @Autowired
    private  ConfigCode confcigCode;
    private static String defaultTest = "-- 选择表名称 --";
    public  void show() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
//        panel = configSocket.buildJpanel(panel);
//        panel = configFilePath.buildJpanel(panel);
        panel = confcigCode.buildJpanel(panel);
        panel = configEvent.buildJpanel(panel);
        panel = configTask.buildJpanel(panel);
        panel = configPrint.buildJpanel(panel);
        buildFrame(panel);
    }

    protected static JButton buildJButton(String name, int x, int y, int width, int height) {
        JButton button = new JButton(name);
        button.setBounds(x, y, width, height);
        return button;
    }

    // 文本框
    protected JTextField buildJTextField(JTextField jtf, String value, String name, int columns, int x, int y, int width, int height) {
        jtf = new JTextField(columns);
        jtf.setText(value);
        jtf.setName(name);
        jtf.setBounds(x, y, width, height);
        return jtf;
    }

    protected static JLabel buildJLabel(String name, int x, int y, int width, int height) {
        JLabel label = new JLabel(name);
        label.setBounds(x, y, width, height);
        return label;
    }

    protected JLabel buildJBorder(String name, int x, int y, int width, int height) {
        JLabel label = new JLabel();
        label.setBounds(x, y, width, height);
        label.setBorder(BorderFactory.createTitledBorder(name));
        return label;
    }

    protected JCheckBox buildJCheckBox(JCheckBox jcb, String value, String name, int columns, int x, int y, int width, int height) {
        jcb = new JCheckBox();
        jcb.setBounds(x, y, width, height);
        jcb.setBorder(BorderFactory.createTitledBorder(name));
        return jcb;
    }

    protected JComboBox buildJComboBox(List<String> tableNames, int x, int y, int width, int height) {
        DefaultComboBoxModel codeTypeModel = new DefaultComboBoxModel();
        // elements 下拉框中的选项
        for (int i = 0; i < tableNames.size(); i++) {
            codeTypeModel.addElement(tableNames.get(i));
        }
        JComboBox codeTypeBox = new JComboBox(codeTypeModel);
        codeTypeBox.setBounds(x, y, width, height);
        return codeTypeBox;
    }

    protected static void saveResult(String msg) {
        if ("true".equals(msg)) {
            JOptionPane.showMessageDialog(null, "保存成功！", "提示", JOptionPane.PLAIN_MESSAGE);
        } else {
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(null, "保存异常！", "提示", JOptionPane.ERROR_MESSAGE);
        }
    }

    private  void buildFrame(JComponent component) {
        JFrame frame = new JFrame("代码生成工具");
        component.setBounds(0, 0, 1200, 600);
        frame.add(component);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().setLayout(null);
        frame.getContentPane().add(BorderLayout.CENTER, component);
        // 设置窗口最小尺寸
        frame.setMinimumSize(new Dimension(1200, 600));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(true);
    }

}
