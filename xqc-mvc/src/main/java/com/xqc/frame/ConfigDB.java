package com.xqc.frame;

import com.xqc.config.PropertiesUtil;
import com.xqc.jdbc.JdbcConnUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.logging.Logger;

@Component
public class ConfigDB {
    private final static java.util.logging.Logger logger = Logger.getLogger("ConfigDB");

    @Autowired
    private ConfigPrint configPrint;
    @Autowired
    PropertiesUtil propertiesUtil;
    @Autowired
    ConfigFrame configFrame;

    private JTextField urlTextField;
    private JTextField usernameTextField;
    private JTextField passwordTextField;
    private JFrame frame;
    public ConfigFrame cf = new ConfigFrame();

    public JPanel buildJpanel(JPanel panel,JFrame frame) {
        this.frame  = frame;
        String url = PropertiesUtil.getConUrl();
        String username = PropertiesUtil.getConUserName();
        String password = PropertiesUtil.getConPassWord();
        int y0 = 0;
        panel.add(cf.buildJBorder("数据库配置", 5, y0 + 10, 490, 180));
        // URL
        panel.add(ConfigFrame.buildJLabel("URL：", 15, y0 + 40, 80, 25));
        urlTextField = cf.buildJTextField(urlTextField, url, "dbUrl", 20, 100, y0 + 40, 350, 25);
        panel.add(urlTextField);
        // 用户名
        panel.add(ConfigFrame.buildJLabel("用户名：", 15, y0 + 70, 80, 25));
        usernameTextField = cf.buildJTextField(usernameTextField, username, "dbUserName", 20, 100, y0 + 70, 165, 25);
        panel.add(usernameTextField);
        // 密码
        panel.add(ConfigFrame.buildJLabel("密码：", 15, y0 + 100, 80, 25));
        passwordTextField = cf.buildJTextField(passwordTextField, password, "dbPassWord", 20, 100, y0 + 100, 165, 25);
        panel.add(passwordTextField);

        // 添加按钮，绑定事件监听
        JButton saveButton = ConfigFrame.buildJButton("保存", 100, y0 + 140, 80, 25);
        addActionListener(saveButton);
        panel.add(saveButton);
        // 添加测试按钮
        JButton testButton = ConfigFrame.buildJButton("测试", 200, y0 + 140, 80, 25);
        addActionListener(testButton);
        panel.add(testButton);
        return panel;
    }

    // 为按钮绑定监听
    private void addActionListener(JButton clickButton) {
        clickButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if("保存".equals(clickButton.getText())){
                            saveActiveEvent();
                        }else if("测试".equals(clickButton.getText())){
                            testActiveEvent();
                        }
                    }
                });
    }

    // save event
    private void saveActiveEvent() {
        this.verify();
        Connection connection = JdbcConnUtils.getConnection();
        if(connection == null){
            JOptionPane.showMessageDialog(null, "数据库连接测试失败！", "提示", JOptionPane.INFORMATION_MESSAGE);
        }else{
            configFrame.show();
            frame.setVisible(false);
        }
    }

    // test event
    private void testActiveEvent() {
        this.verify();
        Connection connection = JdbcConnUtils.getConnection();
        if(connection == null){
            JOptionPane.showMessageDialog(null, "数据库连接测试失败！", "提示", JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null, "数据库连接测试成功！", "提示", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * 参数验证
     */
    private void verify(){
        String textUrl = urlTextField.getText();
        String textUsername = usernameTextField.getText();
        String textPassword = passwordTextField.getText();
        if (StringUtils.isEmpty(textUrl)) {
            JOptionPane.showMessageDialog(null, "请输入数据库URL！", "提示", JOptionPane.INFORMATION_MESSAGE);
        } else if (StringUtils.isEmpty(textUsername)) {
            JOptionPane.showMessageDialog(null, "请输入用户名！", "提示", JOptionPane.INFORMATION_MESSAGE);
        } else if (StringUtils.isEmpty(textPassword)) {
            JOptionPane.showMessageDialog(null, "请输入密码！", "提示", JOptionPane.INFORMATION_MESSAGE);
        }else if(StringUtils.isEmpty(PropertiesUtil.getWriteFilePath())){
            JOptionPane.showMessageDialog(null, "文件输出路径不能为空", "提示", JOptionPane.INFORMATION_MESSAGE);
        } else {
            PropertiesUtil.setConUrl(textUrl);
            PropertiesUtil.setConUserName(textUsername);
            PropertiesUtil.setConPassWord(textPassword);
        }
    }
}
