package com.xqc.utils;

import com.xqc.bean.TableDefinitionBean;
import com.xqc.config.PropertiesUtil;
import com.xqc.constants.XqcConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class MapperUtils {
    /**
     * 接口
     * @param tableName
     */
    public static void createMapper(String tableName){
        String className = ClassUtils.createClassNameSuff(tableName)+"Mapper";
        String entityName = ClassUtils.createClassNameSuff(tableName)+"Entity";
        StringBuilder builder = new StringBuilder("");
        //entity类路径
        String entityInfPth = PropertiesUtil.getPackageName()+"."+XqcConstants.ENTITY_DIR+"."+entityName;
        builder.append("import ")
                .append(entityInfPth)
                .append(";\n")
                .append("import org.springframework.stereotype.Respository;\n")
                .append("import java.util.List;\n")
                .append(ClassUtils.createClassComment(className))
                .append("@Respository \n")
                .append("public interface ")
                .append(className)
                .append(" extends SupperMapper<")
                .append(className)
                .append(" > {\n\n");
        //方法名
        builder.append(selectAll(tableName));
        builder.append(selectByPrimaryKey(tableName));
        builder.append("}");
        try {
            String fileName = FileUtils.createFilePath(XqcConstants.MAPPER_DIR)+"/"+className+".java";
            FileOutputStream fis = new FileOutputStream(new File(fileName));
            fis.write(builder.toString().getBytes());
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(builder.toString());
    }

    /**
     * 查询所有
     * @param tableName
     * @return
     */
    private static String selectAll(String tableName) {
        String entityName = ClassUtils.createClassNameSuff(tableName) +"Entity";
        //entity类路径
        String entityPth = PropertiesUtil.getPackageName() + "." + XqcConstants.ENTITY_DIR;
        StringBuilder mapperMethodBuild = new StringBuilder("\tList<");
        mapperMethodBuild.append(entityName)
                .append("> selectAll(")
                .append(entityName)
                .append(" entity);\n\n");
        return mapperMethodBuild.toString();
    }

    /**
     * 根据主键查询
     * @param tableName
     * @return
     */
    private static String selectByPrimaryKey(String tableName) {
        String entityName = ClassUtils.createClassNameSuff(tableName) +"Entity";
        //entity类路径
        String entityPth = PropertiesUtil.getPackageName() + "." + XqcConstants.ENTITY_DIR;
        StringBuilder mapperMethodBuild = new StringBuilder("\t");
        mapperMethodBuild.append(entityName)
                .append(" selectByPrimaryKey(Integer id);\n\n");
        return mapperMethodBuild.toString();
    }
}
