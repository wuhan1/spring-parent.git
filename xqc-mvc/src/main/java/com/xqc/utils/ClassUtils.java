package com.xqc.utils;

import com.xqc.config.PropertiesUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ClassUtils {

    /**
     * 根据表名称生成实体类前缀
     * @param tableName
     * @return
     */
    public static String createClassNameSuff(String tableName){
        String subTableName = tableName.replaceFirst("t_", "");
        String[] splitStrs = subTableName.split("_");
        StringBuilder builder = new StringBuilder();
        for (String splitStr : splitStrs) {
            char[] charArray = splitStr.toCharArray();
            charArray[0] = Character.toUpperCase(charArray[0]);
            builder.append(new String(charArray));
        }
        return builder.toString();
    }

    /**
     * 类文件注释
     * @param className
     * @return
     */
    public static String createClassComment(String className){
        StringBuilder builder = new StringBuilder("");
        builder.append("/**\n*\n")
                .append("* @author ")
                .append(PropertiesUtil.getAuthor())
                .append("\n")
                .append("* @version $Id: ").append(className)
                .append(".java,v 0.1 ")
                .append(new SimpleDateFormat("yyyy年MM月dd日 HH:mm").format(new Date()))
                .append("   $Exp\n")
                .append("*/\n");
        return builder.toString();
    }


}
