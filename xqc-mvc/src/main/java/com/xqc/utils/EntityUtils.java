package com.xqc.utils;

import com.xqc.bean.TableDefinitionBean;
import com.xqc.constants.XqcConstants;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class EntityUtils {

    public static void CreateEntity(List<TableDefinitionBean> tableDefinitionBeans, String tableName){
        StringBuilder builder = new StringBuilder("");
        String className = ClassUtils.createClassNameSuff(tableName) +"Entity";
        builder.append("")
                .append("import com.baomidou.mybatisplus.annotations.TableId;\n")
                .append("import com.baomidou.mybatisplus.annotations.TableName;\n")
                .append("import com.baomidou.mybatisplus.enums.IdType;\n")
                .append("import com.xsyx.servicestation.jdbc.persistent.AbstractSuperEntity;\n")
                .append("import lombok.Data;\n")
                .append("import java.util.Date;\n\n")
                .append(ClassUtils.createClassComment(className))
                .append("@Data\n@TableName(\"")
                .append(tableName)
                .append("\")\n")
                .append("public class ")
                .append(className)
                .append(" extends AbstractSuperEntity<")
                .append(className)
                .append("> {\n");
        for (TableDefinitionBean tableDefinitionBean : tableDefinitionBeans) {
            String fieldType = TableUtils.subFieldType(tableDefinitionBean.getFieldType());
            String fieldName = tableDefinitionBean.getFieldName();
            //备注为空，则显示字段名
            builder.append("\t/**\n").append("\t*  ")
                    .append(StringUtils.isEmpty(tableDefinitionBean.getComment())?fieldName:tableDefinitionBean.getComment())
                    .append("\n\t*/\n\t");
            //判断是不是主键
            if("PRI".equals(tableDefinitionBean.getKey())){
                builder.append("@TableId(value=\"").append(fieldName).append("\",type=");
                //判断是不是自增
                if("auto_increment".equals(tableDefinitionBean.getExtra())){
                    builder.append("IdType.AUTO)").append("\n\t");
                }else{
                    builder.append("IdType.INPUT)").append("\n\t");
                }
            }else{
                builder.append("@TableField\n\t");
            }
            if("datetime".equals(fieldType) || "date".equals(fieldType)){
                builder.append("private Date ");
            }else if("varchar".equals(fieldType) || "text".equals(fieldType)){
                builder.append("private String ");
            }else if("double".equals(fieldType)){
                builder.append("private Double ");
            }else if("bigint".equals(fieldType)){
                builder.append("private Long ");
            }else if("int".equals(fieldType)){
                builder.append("private Integer ");
            }
            builder.append(fieldName).append(";\n\n");
        }
        builder.append("}");
        try {
            String fileName = FileUtils.createFilePath(XqcConstants.ENTITY_DIR)+"/"+className+ ".java";
            FileOutputStream fis = new FileOutputStream(new File(fileName));
            fis.write(builder.toString().getBytes());
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(builder.toString());
    }

}
