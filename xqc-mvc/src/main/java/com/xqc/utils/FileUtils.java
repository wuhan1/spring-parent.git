package com.xqc.utils;

import com.xqc.config.PropertiesUtil;

import java.io.File;

public class FileUtils {

    /**
     * 创建目录
     * @param fileDir
     * @returnF
     */
    public static String createFilePath(String fileDir){
        String filePath = PropertiesUtil.getWriteFilePath()+"/"+PropertiesUtil.getPackageName().replaceAll("\\.","/")+"/"+ fileDir;
        File file = new File(filePath);
        if(!file.exists()){
            file.mkdirs();
        }
        return file.toPath().toString();
    }

    public static void main(String[] args) {
        System.out.println(createFilePath("abc"));
    }

}
