package com.xqc.utils;

import com.xqc.config.PropertiesUtil;
import com.xqc.constants.XqcConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FacadeUtils {

    /**
     * 接口
     * @param tableName
     */
    public static void createFacadeInf(String tableName){
        String className = ClassUtils.createClassNameSuff(tableName)+"Facade";
        StringBuilder builder = new StringBuilder("");
        builder.append(ClassUtils.createClassComment(className))
                .append("public interface ")
                .append(className)
                .append(" {\n");
        //方法名

        builder.append("}");
        try {
            String fileName = FileUtils.createFilePath(XqcConstants.FACADE_DIR)+"/"+className+".java";
            FileOutputStream fis = new FileOutputStream(new File(fileName));
            fis.write(builder.toString().getBytes());
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(builder.toString());
    }

    /**
     * 实现类
     * @param tableName
     */
    public static void createFacadeInfImpl(String tableName){
        String infName = ClassUtils.createClassNameSuff(tableName)+"Facade";
        String classNameImpl = infName+"Impl";
        StringBuilder builder = new StringBuilder("");
        //facade类路径
        String facadeInfPth = PropertiesUtil.getPackageName()+"."+XqcConstants.FACADE_DIR+"."+infName;
        builder.append("import ")
                .append(facadeInfPth)
                .append(";\n")
                .append(ClassUtils.createClassComment(classNameImpl))
                .append("@Service(version = \"")
                .append(PropertiesUtil.getServiceVersion())
                .append("\")\n")
                .append("@Slf4j\n")
                .append("public class ")
                .append(classNameImpl)
                .append(" implements ")
                .append(infName)
                .append(" {\n");
        //方法名

        builder.append("}");
        try {
            String fileName = FileUtils.createFilePath(XqcConstants.FACADE_IMPL_DIR)+"/"+classNameImpl+".java";
            FileOutputStream fis = new FileOutputStream(new File(fileName));
            fis.write(builder.toString().getBytes());
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(builder.toString());
    }

}
