package com.xqc.utils;

import com.xqc.config.PropertiesUtil;
import com.xqc.constants.XqcConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ServiceUtils {

    public static void createService(String tableName){
        String classNameImpl = ClassUtils.createClassNameSuff(tableName)+"Impl";
        StringBuilder builder = new StringBuilder("");
        builder.append(ClassUtils.createClassComment(classNameImpl))
                .append("@Service\n")
                .append("public class ")
                .append(classNameImpl)
                .append(" {\n");
        //方法名

        builder.append("}");
        try {
            String fileName = FileUtils.createFilePath(XqcConstants.SERVICE_DIR)+"/" +classNameImpl+".java";
            FileOutputStream fis = new FileOutputStream(new File(fileName));
            fis.write(builder.toString().getBytes());
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(builder.toString());
    }

}
