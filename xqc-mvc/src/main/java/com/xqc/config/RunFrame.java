package com.xqc.config;

import com.xqc.frame.ConfigDBFrame;
import com.xqc.frame.ConfigFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class RunFrame implements CommandLineRunner {

    @Autowired
    private ConfigDBFrame concfigDBFrame;

    @Override
    public void run(String... args) throws Exception {
        this.concfigDBFrame.show();
    }
}
