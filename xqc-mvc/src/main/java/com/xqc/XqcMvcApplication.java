package com.xqc;

import com.xqc.frame.ConfigFrame;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
//@Component("com.xqc")
public class XqcMvcApplication {

    public static void main(String[] args) {
        System.out.println("===============开始启动===============");
//        SpringApplication.run(XqcMvcApplication.class,args);
        //桌面图形
        SpringApplicationBuilder builder = new SpringApplicationBuilder(XqcMvcApplication.class);
        builder.headless(false).web(WebApplicationType.NONE).run(args);
        System.out.println("===============启动结束===============");
    }
}
