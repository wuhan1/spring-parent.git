package com.xqc.constants;

public class XqcConstants {

    public static final String ENTITY_DIR = "entity";

    public static final String FACADE_DIR = "facade";

    public static final String FACADE_IMPL_DIR = "facade/impl";

    public static final String SERVICE_DIR = "service/impl";

    public static final String REQUEST_DIR = "request";

    public static final String MAPPER_DIR = "mapper";
}
