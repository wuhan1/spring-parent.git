package com.xqc.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 数据库查询关闭
 * @author SNSSZ00036
 *
 */
public class JdbcCloseUtils {

	private static final Logger logger = LoggerFactory.getLogger(JdbcCloseUtils.class);

	private JdbcCloseUtils(){}
	
	public static  void closeAll(Connection conn,PreparedStatement ps,ResultSet rs){
		close(ps, rs);
		closeCon(conn);
	}
	
	public static  void close(PreparedStatement ps,ResultSet rs){
			if(rs != null){
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error("查询连接关闭失败",e);
				}
			}
			if(ps != null){
				try {
					ps.close();
				} catch (SQLException e) {
					logger.error("查询连接关闭失败",e);
				}
			}
		}
	
	public static  void closeCon(Connection conn){
		if(conn != null){
			try {
				conn.close();
			} catch (Exception e) {
				logger.error("数据库连接关闭失败",e);
			}
		}
	}
}
