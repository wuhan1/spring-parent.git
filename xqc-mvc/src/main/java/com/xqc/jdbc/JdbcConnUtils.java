package com.xqc.jdbc;

import com.xqc.config.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Gavin.Dai
 * @author SNSSZ00036
 */
public class JdbcConnUtils {
	private static final Logger logger = LoggerFactory.getLogger(JdbcConnUtils.class);
	private JdbcConnUtils(){}
	
	/**
	 * 新建一个连接
	 * @return
	 * @throws SQLException 
	 */
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(PropertiesUtil.getConUrl(),PropertiesUtil.getConUserName() ,PropertiesUtil.getConPassWord());
		} catch (Exception e) {
			logger.error("数据库连接失败",e);
		}
		return null;
	}
	
}
