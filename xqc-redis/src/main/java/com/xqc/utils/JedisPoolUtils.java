package com.xqc.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

public class JedisPoolUtils {
    private static final Logger logger = LoggerFactory.getLogger(JedisPoolUtils.class);

    private static final Properties pro = new Properties();
    private JedisPoolUtils(){}

    static {
        InputStream is = null;
        try {
            String rootPath = System.getProperty("user.dir");
            logger.info("redis配置路径{}",rootPath);
            is = new FileInputStream(new File(rootPath + "/redis.properties"));
            pro.load(is);
        } catch (Exception e) {
            logger.info("读取redis配置异常",e);
        }
    }

    /**
     * 源redis
     * @return
     */
    public static JedisPool getSrcJedisPool(Integer dbIndex){
        if(dbIndex == null){
            dbIndex = 0;
        }
        //获得池子对象
        JedisPool srcPool = new JedisPool(getJedisPoolConfig(),  pro.getProperty("src.redis.url"),  Integer.parseInt(pro.getProperty("src.redis.port")), 300000, pro.getProperty("src.redis.password"), dbIndex);
        return srcPool;
    }

    /**
     * 目标redis
     * @return
     */
    private static JedisPool readTargetJedisPool(Integer dbIndex){
        if(dbIndex == null){
            dbIndex = 0;
        }
        //获得池子对象
        JedisPool targetPool = new JedisPool(getJedisPoolConfig(),  pro.getProperty("target.redis.url"),  Integer.parseInt(pro.getProperty("target.redis.port")), 300000, pro.getProperty("target.redis.password"), dbIndex);
        return targetPool;
    }

    private static JedisPoolConfig getJedisPoolConfig(){
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxIdle(Integer.parseInt(pro.getProperty("redis.maxIdle")));//最大闲置个数
        poolConfig.setMinIdle(Integer.parseInt(pro.getProperty("redis.minIdle")));//最小闲置个数
        poolConfig.setMaxTotal(Integer.parseInt(pro.getProperty("redis.maxTotal")));//最大连接数
        return poolConfig;
    }

    /**
     * 获得jedis资源的方法
     */
    public static Jedis getJedis(JedisPool jedisPool) {
    	logger.info("================getSrcJedis执行================");
        return jedisPool.getResource();
    }

    /**
     * 存值：
     */
    public static String set(JedisPool jedisPool,String key,String value){
        return  getJedis(jedisPool).set(key,value);
    }

    /**
     * 取值：
     */
    public static String get(JedisPool jedisPool,String key){
        return getJedis(jedisPool).get(key);
    }

    /**
     * 判断key值是否存在，1存在，0不存在
     */
    public static Boolean exists(JedisPool jedisPool,String key){
        return getJedis(jedisPool).exists(key);
    }

    public static void readDbSelectKeys(JedisPool jedisPool){
        Set<String> keys = getJedis(jedisPool).keys("*");
        for (String key : keys) {
            System.out.println(key);
        }
    }

    public static void copy(JedisPool srcJedisPool,JedisPool targetJedisPool){
        Jedis srcJedis = getJedis(srcJedisPool);
        Jedis targetJedis = getJedis(targetJedisPool);
        Set<String> keys = srcJedis.keys("*");
        for (String key : keys) {
            String type = srcJedis.type(key);
            Long ttl = srcJedis.ttl(key);
            System.out.println(ttl);
            if(type.equals("string")){
                String value = srcJedis.get(key);
                if(ttl == null || Objects.equals(ttl,-1)){
                    targetJedis.set(key,value);
                }else{
                    targetJedis.setex(key,ttl.intValue(),value);
                }
            }else  if(type.equals("hash")){
                String value = srcJedis.hget(key);
                if(ttl == null || Objects.equals(ttl,-1)){
                    targetJedis.set(key,value);
                }else{
                    targetJedis.setex(key,ttl.intValue(),value);
                }
            }else if(type.equals("list")){

            }else if(type.equals("set")){

            }
            System.out.println(key);
        }
    }

}