package com.xqc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xqc.utils.JedisPoolUtils;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.ShardedJedisPool;

import java.util.Set;

public class XqcRedisApplication {
	
    private static final Logger logger = LoggerFactory.getLogger(XqcRedisApplication.class);
	
    public static void main( String[] args ) {

    	logger.info("================开始执行================");
//        String json = JedisPoolUtils.get("json");
//        System.out.println(json);
        logger.info("================读取keys================");
        JedisPool srcJedisPool = JedisPoolUtils.getSrcJedisPool(2);
        JedisPoolUtils.readDbSelectKeys(srcJedisPool);
        logger.info("================读取keys end================");
//        ShardedJedisPool srcJedisPool = JedisPoolUtils.getSrcJedisPool(2);
//        Set<String> hkeys = srcJedisPool.getResource().hkeys("*");
//        for (String hkey : hkeys) {
//            System.out.println(hkey);
//        }

    }
}
