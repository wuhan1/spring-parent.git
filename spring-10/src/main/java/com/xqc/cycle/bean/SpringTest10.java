package com.xqc.cycle.bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

public class SpringTest10 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println(Arrays.toString(context.getBeanDefinitionNames()));
        context.getBean("stuServiceA");
    }
}
