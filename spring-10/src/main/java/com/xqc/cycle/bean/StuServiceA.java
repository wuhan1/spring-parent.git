package com.xqc.cycle.bean;

public class StuServiceA {
    private StuServiceB stuServiceB;


    public StuServiceA(StuServiceB stuServiceB) {
        this.stuServiceB = stuServiceB;
    }

    public StuServiceA() {
        System.out.println("实例化StuServiceA.....");
    }

    public StuServiceB getStuServiceB() {
        return stuServiceB;
    }

    public void setStuServiceB(StuServiceB stuServiceB) {
        this.stuServiceB = stuServiceB;
    }
}
