package com.xqc.cycle.bean;

public class StuServiceB {
    private StuServiceA stuServiceA;

    public StuServiceA getStuServiceA() {
        return stuServiceA;
    }

    public void setStuServiceA(StuServiceA stuServiceA) {
        this.stuServiceA = stuServiceA;
    }

    public StuServiceB(StuServiceA stuServiceA) {
        this.stuServiceA = stuServiceA;
    }

    public StuServiceB() {
        System.out.println("实例化StuServiceB.....");
    }
}
