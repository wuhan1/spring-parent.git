package com.xqc;

import org.springframework.beans.BeanUtils;

public class TestUserMapStruct {

    public static void main(String[] args) {

        User user = new User();
        user.setId(1000);
        user.setAge(21);
        user.setDesc("测试复制工具");
        user.setSalary(400.44);
        user.setUserName("张三");

        UserDTO userDTO = UserMapStruct.INSTANCE.userToDto(user);
        System.out.println(userDTO.toString());

    }
}
