package com.xqc;

import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-12-25T21:30:00+0800",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_181 (Oracle Corporation)"
)
public class UserMapStructImpl implements UserMapStruct {

    @Override
    public UserDTO userToDto(User user) {
        if ( user == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setRemark( user.getDesc() );
        userDTO.setId( user.getId() );
        userDTO.setUserName( user.getUserName() );

        return userDTO;
    }
}
