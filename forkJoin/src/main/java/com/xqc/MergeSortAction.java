package com.xqc;

import java.util.Arrays;
import java.util.concurrent.RecursiveAction;

public class MergeSortAction extends RecursiveAction {

    private int[] arrays;
    private int throdCores;

    public int[] getArrays() {
        return arrays;
    }

    public void setArrays(int[] arrays) {
        this.arrays = arrays;
    }

    public int getThrodCores() {
        return throdCores;
    }

    public void setThrodCores(int throdCores) {
        this.throdCores = throdCores;
    }

    public MergeSortAction(int[] arrays, int throdCores) {
        this.arrays = arrays;
        this.throdCores = throdCores;
    }

    @Override
    protected void compute() {

        int len = arrays.length / 2;

        int[] left = Arrays.copyOfRange(arrays, 0, len);
        int[] right = Arrays.copyOfRange(arrays, len, arrays.length);


    }
}
