package com.xqc;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.cglib.beans.BeanMap;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class Method04 {
    public static void main(String[] args) {
        long s1 = System.currentTimeMillis();
        Map<String, Object> dataMap = objectToMap(new User().setUser());
        long s2 = System.currentTimeMillis();
        System.out.println("花费时间毫秒："+(s2-s1));
        System.out.println(dataMap);
    }

    public static Map<String, Object> objectToMap(Object object){
        Map<String,Object> dataMap = new HashMap<>();
        BeanMap beanMap = BeanMap.create(object);
        for (Object key : beanMap.keySet()) {
            dataMap.put(key+"",beanMap.get(key));
        }
        return dataMap;
    }
}
