package com.xqc;

import java.util.HashMap;
import java.util.Map;

public class Method03 {
    public static void main(String[] args) {
        long s1 = System.currentTimeMillis();
        User user = new User().setUser();
        Map<String,Object> dataMap = new HashMap<>();
        dataMap.put("id",user.getId());
        dataMap.put("userName",user.getUserName());
        dataMap.put("salary",user.getSalary());
        dataMap.put("birthday",user.getBirthday());
        dataMap.put("jobId",user.getJobId());
        long s2 = System.currentTimeMillis();
        System.out.println("花费时间毫秒："+(s2-s1));
        System.out.println(dataMap);
    }
}
