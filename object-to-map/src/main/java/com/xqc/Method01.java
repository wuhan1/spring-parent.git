package com.xqc;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 *使用反射
 */
public class Method01 {
    public static void main(String[] args) {
        long s1 = System.currentTimeMillis();
        Map<String, Object> dataMap = objectToMap(new User().setUser());
        long s2 = System.currentTimeMillis();
        System.out.println("花费时间毫秒："+(s2-s1));
        System.out.println(dataMap);
    }

    public static Map<String, Object> objectToMap(Object object){
        Map<String,Object> dataMap = new HashMap<>();
        Class<?> clazz = object.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                dataMap.put(field.getName(),field.get(object));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return dataMap;
    }
}
