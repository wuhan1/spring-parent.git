package com.xqc;

import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class Method02 {

    public static void main(String[] args) {
        long s1 = System.currentTimeMillis();
        Map<String, String> dataMap = objectToMap(new User().setUser());
        long s2 = System.currentTimeMillis();
        System.out.println("花费时间毫秒："+(s2-s1));
        System.out.println(dataMap);
    }

    public static Map<String, String> objectToMap(Object object){
        try {
            Map<String, String> dataMap = BeanUtils.describe(object);
            dataMap.remove("class");//移除多出来的class字段
            return dataMap;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }
}
