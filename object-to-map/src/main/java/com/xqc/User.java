package com.xqc;

import java.util.Date;

public class User {
    /**
     *
     * ID
     */
    private Integer id;
    /**
     *
     * 用户名
     */
    private String userName;
    /**
     *
     * 岗位ID
     */
    private Long jobId;
    /**
     * 生日
     */
    private Date birthday;
    /**
     *
     * 薪水
     */
    private Double salary;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public User setUser(){
        User user = new User();
        user.setId(123);
        user.setUserName("王二");
        user.setSalary(2344.4D);
        user.setBirthday(new Date());
        user.setJobId(99999L);
        return user;
    }
}
