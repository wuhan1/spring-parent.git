package com.xqc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XqcAppMain {
    public static void main(String[] args) {
        SpringApplication.run(XqcAppMain.class,args);
    }
}
