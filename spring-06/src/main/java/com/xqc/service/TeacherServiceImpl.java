package com.xqc.service;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Service
public class TeacherServiceImpl {

    @PostConstruct
    public void myInit(){
        System.out.println("老师初始化");
    }

    @PreDestroy
    public void myDestroy(){
        System.out.println("老师销毁");
    }
}
