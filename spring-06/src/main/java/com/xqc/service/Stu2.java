package com.xqc.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Stu2 {

    /**
     * 初始化方法
     */
    @PostConstruct
    public void myInit(){
        System.out.println("学生初始化2");
    }

    /**
     * 销毁方法
     */
    @PreDestroy
    public void myDestroy(){
        System.out.println("学生销毁2");
    }
}
