package com.xqc.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    /**
     * 拦截所有的bean
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//        if(bean instanceof Stu4){
        System.out.println("MyBeanPostProcessor初始化4");
//        }MyBeanPostProcessor
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        if(bean instanceof Stu4){
        System.out.println("MyBeanPostProcessor销毁4");
//        }
        System.out.println(bean);
        return bean;
    }
}
