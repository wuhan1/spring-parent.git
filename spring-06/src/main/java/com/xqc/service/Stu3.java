package com.xqc.service;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Stu3 implements InitializingBean, DisposableBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("学生初始化3");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("学生销毁3");
    }
}
