package com.xqc.aware;

import com.xqc.service.StuServiceImpl;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class MyApplicationContextAware implements ApplicationContextAware {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("---"+applicationContext);
//        StuServiceImpl stuServiceImpl = (StuServiceImpl) applicationContext.getBean("stuServiceImpl");
//        System.out.println(stuServiceImpl);
    }
}
