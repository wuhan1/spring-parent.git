package com.xqc.config;

import com.xqc.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.xqc")//扫描的包
public class SpringConfig {

//    @Bean(initMethod = "myInit",destroyMethod = "myDestroy")
//    public Stu stu(){
//        return new Stu();
//    }

//    @Bean
//    public Stu2 stu2(){
//        return new Stu2();
//    }

//    @Bean
//    public Stu3 stu3(){
//        return new Stu3();
//    }

    @Bean
    public Stu4 stu4(){
        return new Stu4();
    }
}
